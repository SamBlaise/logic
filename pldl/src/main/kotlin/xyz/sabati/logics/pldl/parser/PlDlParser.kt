package xyz.sabati.logics.pldl.parser

import xyz.sabati.logics.pldl.syntax.PlDefaultRule
import xyz.sabati.logics.pldl.syntax.PlDefaultTheory
import net.sf.tweety.commons.Formula
import net.sf.tweety.commons.Parser
import net.sf.tweety.commons.ParserException
import net.sf.tweety.logics.pl.parser.PlParser
import net.sf.tweety.logics.pl.syntax.PlBeliefSet
import net.sf.tweety.logics.pl.syntax.PropositionalFormula
import net.sf.tweety.logics.pl.syntax.Tautology
import java.io.Reader

/**
 * Parser for Propositional Default Theory
 * @author Samuel Batissou
 */
class PlDlParser : Parser<PlDefaultTheory>() {

    private val parser: PlParser = PlParser()

    companion object {
        private const val DIV_COLON = ":"
        private const val DIV_COMMA = ","
    }

    override fun parseFormula(reader: Reader?): Formula {
        TODO("not implemented")
    }

    /**
     * Parse from a reader a PlDefaultTheory.
     * @param reader
     */
    override fun parseBeliefBase(reader: Reader?): PlDefaultTheory {
        if (reader == null)
            throw ParserException("The reader is null")

        val lines = reader.readLines()
        val facts = PlBeliefSet(lines.filter { it.startsWith("fact") }
            .map { parser.parseFormula(it.removePrefix("fact(").removeSuffix(")")) as PropositionalFormula })

        val plDefaultFormulae = lines.filter { it.startsWith("default") }
            .map { it.removePrefix("default(").removeSuffix(")") }
            .map { it.split(DIV_COLON) }
            .map {
                val x = if (it[0].isEmpty()) Tautology() else parser.parseFormula(it[0]) as PropositionalFormula
                val y = it[1].split(DIV_COMMA).map { s -> parser.parseFormula(s) as PropositionalFormula }
                val z = if (it[2].isEmpty()) Tautology() else parser.parseFormula(it[2]) as PropositionalFormula
                PlDefaultRule(x, y, z)
            }
        return PlDefaultTheory(plDefaultFormulae, facts)
    }
}