package xyz.sabati.logics.pldl.semactics

import xyz.sabati.logics.pldl.syntax.PlDefaultTheory
import java.util.*

/**
 * Class to generate extension from a DefaultTheory
 * @author  Samuel Batissou
 */
class PlDefaultProcessTree(defaultTheory: PlDefaultTheory) {
    private val processes: MutableCollection<PlDefaultSequence> = mutableListOf()
    val extensions: MutableCollection<PlExtension> = mutableListOf()

    init {
        var seqsOld: MutableList<PlDefaultSequence> = ArrayList()
        var seqsNew: MutableList<PlDefaultSequence> = ArrayList()
        seqsOld.add(PlDefaultSequence(defaultTheory))
        while (!seqsOld.isEmpty()) {
            for (seqOld in seqsOld) {
                for (defaultRule in defaultTheory.plDefaultFormulae) {
                    val seqNew = seqOld.apply(defaultRule)
                    if (seqNew.process)
                        if (seqNew.isSuccessful())
                            if (seqNew.isClosed(defaultTheory))
                                processes.add(seqNew)
                            else
                                seqsNew.add(seqNew)
                }
            }
            seqsOld = seqsNew
            seqsNew = ArrayList()
        }
        for (seq in processes)
            extensions.add(PlExtension(seq.input))
    }
}