package xyz.sabati.logics.pldl.util

import net.sf.tweety.logics.pl.syntax.PlBeliefSet
import net.sf.tweety.logics.pl.syntax.PropositionalFormula
import xyz.sabati.logics.pldl.syntax.PlDefaultRule
import xyz.sabati.logics.pldl.syntax.PlDefaultTheory

class PlDefaultTheoryBuilder {
    private val facts = PlBeliefSet()
    private val defaultRules = mutableListOf<PlDefaultRule>()

    fun addFact(fact: PropositionalFormula): PlDefaultTheoryBuilder {
        facts.add(fact)
        return this
    }

    fun addDefaultRule(dfr: PlDefaultRule): PlDefaultTheoryBuilder {
        defaultRules.add(dfr)
        return this
    }

    fun build(): PlDefaultTheory {
        return PlDefaultTheory(defaultRules, facts)
    }

    fun clear(): PlDefaultTheoryBuilder {
        return PlDefaultTheoryBuilder()
    }
}