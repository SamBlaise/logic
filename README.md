# Propositional Default Logic Tweety Extension

I used the tweety project java library. I added the
functionality to generates default logic extensions 
for propositional logic.


The module **pldl** contains all the classes for 
managing the default theory.

the module **extcal** contains a cli that takes a file 
in argument and prints all the extension.

API Documentation : 

https://samblaise.gitlab.io/logic/